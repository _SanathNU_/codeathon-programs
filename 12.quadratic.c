#include<stdio.h>
#include<math.h>
int main()
{
	int a,b,c;
	float d,x1,x2,i;
	printf("Enter the values of coefficients:\n");
	scanf("%d%d%d",&a,&b,&c);
	d=pow(b,2)-4*a*c;
	switch(d>0)
	{
		case 1:
		x1=(-b+sqrt(d))/(2*a);
		x2=(-b-sqrt(d))/(2*a);
		printf("Two real and distinct roots exist:%f & %f\n",x1,x2);
	    break;
		
		case 0:
		switch(d<0)
		{
			case 1:
			x1=x2=-b/(2*a);
			i=sqrt(-d)/(2*a);
			printf("Two roots are imaginary:%f+i%f & %f-i%f\n",x1,i,x2,i);
			break;
			
			case 0:
			x1=x2=-b/(2*a);
			printf("Two real and equal roots exist:%f & %f\n",x1,x2);
			break;
		}
	}
	return 0;
}